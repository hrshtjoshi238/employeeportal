import React,{Component} from 'react';
import auth from '../services/authservice';
import http from '../services/httpService';
class Login extends Component{
    state={
        form:{
            email:"",
            password:""
        }
    };
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.form[input.name]=input.value;
        this.setState(s1);
    };
    async loginUser(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            //console.log(data);
            auth.login(data);
            //this.props.history.push("/products");
            //console.log("Success\n"+response);
            data.role==="ADMIN"?window.location="/admin":window.location="/emp";
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors={};
                //console.log(ex.response.data)
                errors.email=ex.response.data;
                this.setState({errors:errors});
            }
        }
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        this.loginUser("/empapp/loginuser",this.state.form);
    }
    render(){
    let {email,password}=this.state.form;
    let {errors=null}=this.state;
    return(
        <div className="container text-center">
            <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <h3 className="text-center">Login</h3>
            <div className="row bg-light p-4">
            {
                    errors&&errors.email&&(<span className="col-12 text-danger">Login Failed. Check the email and password</span>)
            }
                <div className="col-6 text-right"><label htmlFor="email">Email:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="email"
                        className="form-control"
                        type="text"
                        name="email"
                        placeholder="Enter Your Email Id"
                        value={email}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="password">Password:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="password"
                        className="form-control"
                        type="password"
                        name="password"
                        placeholder="Enter Your Password"
                        value={password}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-12 text-center">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </div>
            </div>
        </div>
    )
}
}
export default Login;
