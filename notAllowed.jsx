import React,{Component} from 'react';
class NotAllowed extends Component{
    render(){
        let {user}=this.props;
        return(
            <div className="container">
                <p><b>Functionality is only accessable to admin</b></p>
            </div>
        )
    }
}
export default NotAllowed;