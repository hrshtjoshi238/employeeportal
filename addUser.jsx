import React,{Component} from 'react';
import http from '../services/httpService';
class AddUser extends Component{
    state={
        user:{
            name:"",
            email:"",
            password:"",
            password2:"",
            role:"EMPLOYEE"
        },
        errors:{}
    }
    validateAll=()=>{
        let {name,email,password,password2}=this.state.user;
        let errors={};
        errors.name=this.validateName(name);
        errors.email=this.validateEmail(email);
        errors.password=this.validatePassword(password);
        errors.password2=this.validatePassword2(password2);
        return errors;

    }
    validateName=(name)=>{
        return !name?"Name must be entered":
                name.length<8?"Name should have atleast 8 characters":
                "";
    }
    validateEmail=(email)=>{
        return !email?"Email must be entered":
        email.indexOf("@")===-1?"Enter valid email":
        "";
    }
    validatePassword=(password)=>{
        let v1 = /^(?=.*[a-z])/;
        let v2 = /^(?=.*[A-Z])/;
        let v3 = /^(?=.*[0-9])/
        return !password?"Password must be entered":
        password.length<8?"Password should have atleast 8 characters":
        v1.test(password)==false?"Invalid Password":
        v2.test(password)==false?"Invalid Password":
        v3.test(password)==false?"Invalid Password":
        "";
        
    }
    validatePassword2=(password2)=>{
        let {password}=this.state.user;
        return password2!=password?"Password didn't match":
        "";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.user[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit = (e) =>{
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            //console.log(errors);
            let {user}=this.state;
            let emp={
                name:user.name,
                email:user.email,
                password:user.password,
                role:user.role
            }
            this.loginUser("/empapp/emps",emp);
        }
        
    }
    async loginUser(url,obj){
        try {
            let response= await http.post(url,obj);

            let errors={name:"",email:"",password:"",password2:""};
            let {data}=response;
            //console.log(data);
            this.setState({success:data,errors:errors});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    render(){
    let {name,email,password,password2}=this.state.user;
    let {errors,success,errors2}=this.state;
    return(
        <div className="container text-center">
            <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <h3 className="text-center">Add User</h3>
            <div className="row bg-light p-4">
                <div className="col-12">
                    {success?<p className="text-success">Employee successfully added.</p>:
                    errors2?<p className="text-danger">{errors2}</p>:
                    ""}
                </div>
                <div className="col-6 text-right"><label htmlFor="name">Name:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="name"
                        className="form-control"
                        type="text"
                        name="name"
                        placeholder="Enter Your Name"
                        value={name}
                        onChange={this.handleChange}
                        />
                        {errors.name?<p className="text-danger text-left">{errors.name}</p>:""}
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="email">Email:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="email"
                        className="form-control"
                        type="text"
                        name="email"
                        placeholder="Enter Your Email Id"
                        value={email}
                        onChange={this.handleChange}
                        />
                        {errors.email?<p className="text-danger text-left">{errors.email}</p>:""}
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="email">Password:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="password"
                        className="form-control"
                        type="password"
                        name="password"
                        placeholder="Enter Password"
                        value={password}
                        onChange={this.handleChange}
                        />
                        {errors.password?<p className="text-danger text-left">{errors.password}</p>:""}
                    </div>
                </div>
                <div className="col-6 text-right"></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        className="form-control"
                        type="password"
                        name="password2"
                        placeholder="Re-Enter Your Password"
                        value={password2}
                        onChange={this.handleChange}
                        />
                        {errors.password2?<p className="text-danger text-left">{errors.password2}</p>:""}
                    </div>
                </div>
                
                <div className="col-12 text-center">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </div>
            </div>
        </div>
    )
}

}
export default AddUser;