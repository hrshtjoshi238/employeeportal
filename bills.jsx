import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservice';
class Bills extends Component{
    state={
        billDetails:{},
        enterBill:false,
        expenseTypeArr:["Travel","Hotel","Software","Communication","Others"],
        inputDetails:{
            empuserid:"",
            description:"",
            expensetype:"",
            amount:""
        },
        errors:{}
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.inputDetails[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let user=auth.getUser();
        let {empuserid}=user;
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            //console.log(errors);
            this.addDetails(`/empapp/empbills/${empuserid}`,this.state.inputDetails);
        }

    }
    validateAll=()=>{
        let {description,expensetype,amount}=this.state.inputDetails;
        let errors={};
        errors.description=this.validateDescription(description);
        errors.expensetype=this.validateExpenseType(expensetype);
        errors.amount=this.validateAmount(amount);
        return errors;

    }
    validateDescription=(description)=>!description?"Description is not entered":"";
    validateExpenseType=(expensetype)=>!expensetype?"Expense Type is not selected":"";
    validateAmount=(amount)=>{
        if(!amount){
            return "Amount is not entered"; 
        }
        if(amount.indexOf(".")!=-1){
            if(amount.split("").reduce((acc,curr)=>curr==="."?++acc:acc,0)>1){
                return "Enter valid amount";
            }
            if(!amount.split(".")[1]){
                return "Enter valid amount"
            }
            return "";
        }
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    async componentDidMount(){
        let user=auth.getUser();
        let {empuserid}=user;
        //console.log(empuserid);
        try {
            let response=await http.get(`/empapp/empbills/${empuserid}`);
            let {data}=response;
            this.setState({billDetails:data,detailsAvail:true});
        }
        catch (ex){
            if(ex.response&&ex.response.status==500){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({detailsAvail:false});
            }
        }
    }
    async addDetails(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            let errors={description:"",expensetype:"",amount:""};
            //console.log(response);
            this.setState({success:data,errors:errors});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    showBillForm=()=>{
        let s1={...this.state};
        s1.enterBill===false?s1.enterBill=true:s1.enterBill=false;
        this.setState(s1);
    };
    goToBill=(billId,billType)=>{
        let user=auth.getUser();
        let {empuserid}=user;
        billType==="Hotel"?window.location=`/emp/hotelbill/${billId}`:window.location=`/emp/travelbill/${billId}`;
    }
    render(){
        let {billDetails={},detailsAvail,enterBill,expenseTypeArr,inputDetails,errors,errors2,success}=this.state;
        let {description,expensetype,amount}=inputDetails;
        let {data=[]}=billDetails
        //console.log(billDetails)
        return(
            <div className="container">
                <h3 className="text-center">Welcome To Employee Management Portal</h3>
                <h4>Details of Bill Submitted</h4>
                <div className="row bg-primary">
                    <div className="col-2 text-center border">Id</div>
                    <div className="col-4 text-center border">Description</div>
                    <div className="col-3 text-center border">Expense Head</div>
                    <div className="col-2 text-center border">Amount</div>
                    <div className="col-1 text-center border"></div>
                </div>
                {data.map((x)=>(
                    <div className="row">
                        <div className="col-2 text-center border">{x.billid}</div>
                        <div className="col-4 text-center border">{x.description}</div>
                        <div className="col-3 text-center border">{x.expensetype}</div>
                        <div className="col-2 text-center border">{x.amount}</div>
                        <div className="col-1 text-center border">{x.expensetype==="Hotel"||x.expensetype==="Travel"?<i className="fa fa-plus-square" onClick={()=>this.goToBill(x.billid,x.expensetype)} aria-hidden="true"></i>:""}</div>
                    </div>
                ))}
                <a className="text-dark" onClick={()=>this.showBillForm()} style={{textDecorationColor:"black",cursor:"pointer"}}>Submit a New Bill</a>
                {
                    enterBill?<div className="bg-light">
                        <h4 className="text-center">Enter Details of the new Bill</h4>
                        <div className="row">
                            {
                                success?<div className="col-12 text-success text-center">Bill Successfully Added</div>:
                                errors2?<div className="col-12 text-danger text-center">{errors2}</div>:
                                ""
                            }   
                            <label className="col-6 text-right" htmlFor="description">Description:</label>
                            <div className="form-group col-3"><input
                            className="form-control"
                            type="text"
                            id="description"
                            value={description}
                            name="description"
                            onChange={this.handleChange}/>
                            {errors.description?<p className="text-danger text-left">{errors.description}</p>:""}
                            </div>
                            
                            <label className="col-6 text-right" htmlFor="expensetype">Expense Type:</label>
                            <div className="form-group col-3"><select className="form-control" id="expenstype" name="expensetype" value={expensetype} onChange={this.handleChange}>
                                <option disabled selected value="">Select Expense Type</option>
                                {
                                    expenseTypeArr.map((x)=><option>{x}</option>)
                                }
                            </select>
                            {errors.expensetype?<p className="text-danger text-left">{errors.expensetype}</p>:""}
                            </div>

                            <label className="col-6 text-right" htmlFor="amount">Amount:</label>
                            <div className="form-group col-3"><input
                            className="form-control"
                            type="text"
                            id=""
                            value={amount}
                            name="amount"
                            onChange={this.handleChange}/>
                            {errors.amount?<p className="text-danger text-left">{errors.amount}</p>:""}
                            </div>
                            <div className="col-12 text-center">
                                <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                            </div>
                        </div>
                    </div>:""
                }
            </div>
        )
    }
}
export default Bills; 