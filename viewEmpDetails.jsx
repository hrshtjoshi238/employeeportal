import React,{Component} from 'react';
import http from '../services/httpService';
class ViewEmpDetails extends Component{
    state={
        departmentDetails:{}
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.departmentDetails[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit = (e) =>{
        let {id}=this.props.match.params;
        e.preventDefault();
        this.addDetails(`/empapp/empdept/${id}`,this.state.departmentDetails);
        
    }
    async componentDidMount(){
        let {id}=this.props.match.params;
        let response=await http.get(`/empapp/empdept/${id}`);
        let {data}=response;
        //console.log(data);
        let detailsAvail=false;
        data.manager&&data.department&&data.designation?detailsAvail=true:detailsAvail=false;
        this.setState({departmentDetails:data,detailsAvail:detailsAvail});
    }
    async addDetails(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            //console.log(response);
            this.setState({success:data,detailsAvail:false});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    render(){
        let {departmentDetails={},detailsAvail,success,errors2}=this.state;
        let {manager="",designation="",department=""}=departmentDetails;
        return(
            <div className="container text-center">
                <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <h3 className="text-center">Department Details of New Employee</h3>
            <div className="row bg-light p-4">
            {
                    detailsAvail?<span className="col-12 text-success">Displaying Department Details</span>:
                    success?<span className="col-12 text-success">Details Successfully Added</span>:
                    errors2?<span className="col-12 text-danger">{errors2}</span>:
                    <span className="col-12 text-danger">No Department Details Found. Please Enter Them</span>
            }
                <div className="col-6 text-right"><label htmlFor="department">Department:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="department"
                        className="form-control"
                        type="text"
                        name="department"
                        placeholder="Enter Department"
                        value={department}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="designation">Designation:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="designation"
                        className="form-control"
                        type="text"
                        name="designation"
                        placeholder="Enter Designation"
                        value={designation}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="manager">Manager's Name:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="manager"
                        className="form-control"
                        type="text"
                        name="manager"
                        placeholder="Enter Manager's Name:"
                        value={manager}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                
                <div className="col-12 text-center">
                    <button className="btn btn-primary" onClick={this.handleSubmit} disabled={detailsAvail}>Submit</button>
                </div>
            </div>
            </div>
        )
    }
}
export default ViewEmpDetails;