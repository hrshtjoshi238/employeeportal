import React,{Component} from 'react';
import {Route,Switch,Redirect} from 'react-router-dom';
import NavBar from './navBar';
import Login from './login';
import Logout from './logout';
import auth from '../services/authservice';
import ShowPage from './showPage';
import NotAllowed from './notAllowed';
import ViewEmp from './viewEmp';
import AddUser from './addUser';
import ViewEmpDetails from './viewEmpDetails';
import Contact from './contact';
import Bills from './bills';
import HotelBill from './hotelBill';
import FlightBill from './flightBill';
class MainComp extends Component{
    render(){
        const user=auth.getUser();
        console.log(user);
        return(
            <React.Fragment>
                <NavBar user={user}/>
                <Switch>
                    <Route path="/admin/viewemp/:id" render={(props)=>user&&user.role==="ADMIN"?<ViewEmpDetails {...props}/>:<Redirect to="/notAllowed"/>}/>
                    <Route path="/admin/viewemp" render={(props)=>user&&user.role==="ADMIN"?<ViewEmp {...props}/>:<Redirect to="/notAllowed"/>}/>
                    <Route path="/admin/addUser" render={(props)=>user&&user.role==="ADMIN"?<AddUser {...props}/>:<Redirect to="/notAllowed"/>}/>
                    <Route path="/emp/hotelbill/:billId" component={HotelBill}/>
                    <Route path="/emp/travelbill/:billId" component={FlightBill}/>
                    <Route path="/emp/bills" render={(props)=>user&&user.role!="ADMIN"?<Bills {...props}/>:<Redirect to="/notAllowed"/>}/>
                    <Route path="/emp/contact" render={(props)=>user&&user.role!="ADMIN"?<Contact {...props}/>:<Redirect to="/notAllowed"/>}/>
                    {user&&user.role==="ADMIN"&&<Route path="/admin" render={(props)=><ShowPage {...props} user={user}/>}/>}
                    {user&&user.role!="ADMIN"&&<Route path="/emp" render={(props)=><ShowPage {...props} user={user}/>}/>}
                    <Route path="/login" render={(props)=><Login {...props}/>}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/notAllowed" component={NotAllowed} user={user}/>
                    {user&&user.role==="ADMIN"&&<Redirect from="/" to="/admin"/>}
                    {user&&user.role!="ADMIN"&&<Redirect from="/" to="/emp"/>}
                    {!user&&<Redirect from="/" to="/login"/>}
                </Switch>
            </React.Fragment>
        )
    }
}
export default MainComp;