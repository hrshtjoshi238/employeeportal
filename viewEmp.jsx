import React,{Component} from "react";
import http from "../services/httpService";
class ViewEmp extends Component{
    state={
        emps:{}
    }
    async componentDidMount(){
        let response=await http.get("/empapp/emps");
        let {data}=response;
        this.setState({emps:data});
    }
    getDetails=(id)=>{
        this.props.history.push(`/admin/viewemp/${id}`)
    }
    render(){
        const {emps={}}=this.state;
        const {data=[],pageInfo={}}=emps;
        //console.log(emps);
        return(
            <div className="container">
                <div className="row bg-primary text-center text-white">
                    <div className="col-4 border">Name</div>
                    <div className="col-4 border">Email Id</div>
                    <div className="col-4 border"></div>
                </div>
                {
                    data.map((x)=>(
                        <div className="row text-center">
                        <div className="col-4 border">{x.name}</div>
                        <div className="col-4 border">{x.email}</div>
                        <div className="col-4 border">
                            <button className="btn btn-secondary btn-sm"
                            onClick={()=>this.getDetails(x.empuserid)}>
                                Details
                            </button>
                        </div>
                        </div>  
                    ))
                }
            </div>
        )
    }
}
export default ViewEmp;