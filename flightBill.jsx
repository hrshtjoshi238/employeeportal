import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservice';
class FlightBill extends Component{
    state={
        flightDetails:{
            goflightDate:{
                day:"",
                month:"",
                year:"",
            },
            goflightOrigin:"",
            goflightDest: "",
            goflightNum: "",
            backflightDate:{
                day:"",
                month:"",
                year:"",
            },
            backflightOrigin: "",
            backflightDest: "",
            backflightNum: "",
            corpbooking: ""
        },
        years:["2018","2019","2020"],
        months:["January","February","March","April","May","June","July","August","September","October","November","December"],
        errors:{},
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        input.className.split(" ")[1]==="goflightDate"||input.className.split(" ")[1]==="backflightDate"?
            s1.flightDetails[input.className.split(" ")[1]][input.name]=input.value
        :input.type==="checkbox"?
            input.checked?s1.flightDetails[input.name]="Yes"
            :s1.flightDetails[input.name]="No"
        :s1.flightDetails[input.name]=input.value;
        this.setState(s1);
    }
    validateAll=()=>{
        let {goflightDate,goflightOrigin,goflightDest,goflightNum,backflightDate,backflightOrigin,backflightDest,backflightNum}=this.state.flightDetails;
        let errors={};
        errors.goflightOrigin=!goflightOrigin?"Origin is Not Entered":"";
        errors.goflightDest=!goflightDest?"Destination is Not Entered":"";
        errors.goflightNum=!goflightNum?"Flight Number is Not Entered":"";
        errors.goflightOrigin=!backflightOrigin?"Origin is Not Entered":"";
        errors.goflightDest=!backflightDest?"Destination is Not Entered":"";
        errors.goflightNum=!backflightNum?"Flight Number is Not Entered":"";
        errors.goflightDate=this.validateGoFlightDate(goflightDate);
        errors.backflightDate=this.validateBackFlightDate(backflightDate);
        return errors;
    }
    validateGoFlightDate=(goflightDate)=>{
        let keys=Object.keys(goflightDate);
        return keys.reduce((acc,curr)=>!goflightDate[curr]?++acc:acc,0)?"Date is not selected properly":"";
    }
    validateBackFlightDate=(backflightDate)=>{
        let keys=Object.keys(backflightDate);
        return keys.reduce((acc,curr)=>!backflightDate[curr]?++acc:acc,0)?"Date is not selected properly":"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit=(e)=>{
        let {billId}=this.props.match.params;
        e.preventDefault();
        let user=auth.getUser();
        let {empuserid}=user;
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            let s1={...this.state};
            let flightDetails={
                billid:billId,
                empuserid:empuserid,
                goflightDest:s1.flightDetails.goflightDest,
                goflightOrigin:s1.flightDetails.goflightOrigin,
                goflightNum:s1.flightDetails.goflightNum,
                backflightDest:s1.flightDetails.backflightDest,
                backflightOrigin:s1.flightDetails.backflightOrigin,
                backflightNum:s1.flightDetails.backflightNum,
                goflightDate:s1.flightDetails.goflightDate.day+"-"+s1.flightDetails.goflightDate.month+"-"+s1.flightDetails.goflightDate.year,
                backflightDate:s1.flightDetails.backflightDate.day+"-"+s1.flightDetails.backflightDate.month+"-"+s1.flightDetails.backflightDate.year,
                corpbooking:s1.flightDetails.corpbooking?"Yes":"No"
            }
            this.addDetails(`/empapp/travelbill`,flightDetails);
        }
    }
    async addDetails(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            let errors={};
            //console.log(response);
            this.setState({success:data,detailsAvail:false,errors:errors});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    async componentDidMount(){
        let {billId}=this.props.match.params;
        let user=auth.getUser();
        let {empuserid}=user;
        //console.log(empuserid);
        try {
            let response=await http.get(`/empapp/travelbill/${empuserid}/${billId}`);
            let {data}=response;
            //console.log(data);
            let details={
                goflightDate:{
                    day:data.goflightDate.split("-")[0],
                    month:data.goflightDate.split("-")[1],
                    year:data.goflightDate.split("-")[2],
                },
                goflightOrigin:data.goflightOrigin,
                goflightDest:data.goflightDest,
                goflightNum:data.goflightNum,
                backflightDate:{
                    day:data.backflightDate.split("-")[0],
                    month:data.backflightDate.split("-")[1],
                    year:data.backflightDate.split("-")[2],
                },
                backflightOrigin:data.backflightOrigin,
                backflightDest:data.backflightDest,
                backflightNum:data.backflightNum,
                corpbooking:data.corpbooking
            }
            this.setState({
                flightDetails:details,
                detailsAvail:true
            });
        }
        catch (ex){
            if(ex.response&&ex.response.status==500){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({detailsAvail:false});
            }
        }
    }
    getDays=()=>{
        let arr=[];
        for(let i=1;i<=31;i++){
            arr.push(i);
        }
        return arr;
    }
    render(){
        let {flightDetails,years,months,detailsAvail,success,errors2,errors}=this.state;
        let {goflightDate,goflightOrigin,goflightDest,goflightNum,backflightDate,backflightOrigin,backflightDest,backflightNum,corpbooking}=flightDetails;
        let days=this.getDays();
        let {billId}=this.props.match.params;
        return (
        <div className="container">
            <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <div className="row bg-light p-2">
                {
                    detailsAvail?<span className="col-12 text-success text-center">Displaying Flight Details</span>:
                    success?<span className="col-12 text-success text-center">Details Successfully Added</span>:
                    errors2?<span className="col-12 text-danger text-center">{errors2}</span>:
                    <span className="col-12 text-primary text-center">No Flight Details Found. Please Enter Them</span>
                }    
                <h4 className="col-12 text-center">Flight Details</h4>
                <h6 className="col-12 text-center font-weight-bolder">Bill Id : {billId}</h6>
                <hr className="col-11"/>
                <span className="col-12 text-center font-weight-bolder">Departure Flight Details</span>
                <label className="col-6 text-right">Flight Date:</label>
                    <div className="col-6">
                    <div className="row">
                    <div className="form-group col-4">
                        <select className="form-control goflightDate" disabled={detailsAvail} name="day" value={goflightDate.day} onChange={this.handleChange}>
                            <option disabled selected value="">Select Day</option>
                            {
                                days.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control goflightDate"  disabled={detailsAvail} name="month" value={goflightDate.month} onChange={this.handleChange}>
                            <option disabled selected value="">Select Month</option>
                            {
                                months.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control goflightDate" disabled={detailsAvail} name="year" value={goflightDate.year} onChange={this.handleChange}>
                            <option disabled selected value="">Select Year</option>
                            {
                                years.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    {
                        errors.goflightDate?<p className="text-danger text-left">{errors.goflightDate}</p>:""
                    }
                    </div>
                    </div>
                <label className="col-6 text-right" htmlFor="goflightOrigin">Origin City:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="goflightOrigin"
                            value={goflightOrigin}
                            name="goflightOrigin"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.goflightOrigin?<p className="text-danger text-left">{errors.goflightOrigin}</p>:""
                            }
                        </div>
                <label className="col-6 text-right" htmlFor="goflightDest">Destination City:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="goflightDest"
                            value={goflightDest}
                            name="goflightDest"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.goflightDest?<p className="text-danger text-left">{errors.goflightDest}</p>:""
                            }
                        </div>
                <label className="col-6 text-right" htmlFor="goflightNum">Flight Number:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="goflightNum"
                            value={goflightNum}
                            name="goflightNum"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.goflightNum?<p className="text-danger text-left">{errors.goflightNum}</p>:""
                            }
                        </div>
                <hr className="col-11"/>
                <span className="col-12 text-center font-weight-bolder">Return Flight Details</span>
                <label className="col-6 text-right">Flight Date:</label>
                    <div className="col-6">
                    <div className="row">
                    <div className="form-group col-4">
                        <select className="form-control backflightDate" disabled={detailsAvail} name="day" value={backflightDate.day} onChange={this.handleChange}>
                            <option disabled selected value="">Select Day</option>
                            {
                                days.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control backflightDate"  disabled={detailsAvail} name="month" value={backflightDate.month} onChange={this.handleChange}>
                            <option disabled selected value="">Select Month</option>
                            {
                                months.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control backflightDate" disabled={detailsAvail} name="year" value={backflightDate.year} onChange={this.handleChange}>
                            <option disabled selected value="">Select Year</option>
                            {
                                years.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    {
                        errors.backflightDate?<p className="text-danger text-left">{errors.backflightDate}</p>:""
                    }
                    </div>
                    </div>
                <label className="col-6 text-right" htmlFor="backflightOrigin">Origin City:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="backflightOrigin"
                            value={backflightOrigin}
                            name="backflightOrigin"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.backflightOrigin?<p className="text-danger text-left">{errors.backflightOrigin}</p>:""
                            }
                        </div>
                <label className="col-6 text-right" htmlFor="backflightDest">Destination City:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="backflightDest"
                            value={backflightDest}
                            name="backflightDest"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.backflightDest?<p className="text-danger text-left">{errors.backflightDest}</p>:""
                            }
                        </div>
                <label className="col-6 text-right" htmlFor="backflightNum">Flight Number:</label>
                        <div className="form-group col-3">
                            <input
                            className="form-control"
                            type="text"
                            id="backflightNum"
                            value={backflightNum}
                            name="backflightNum"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.backflightNum?<p className="text-danger text-left">{errors.backflightNum}</p>:""
                            }
                        </div>
                        <div className="form-group text-right col-6"><input
                            className="form-checked"
                            type="checkbox"
                            id="corpbooking"
                            value={corpbooking}
                            checked={corpbooking==="Yes"}
                            name="corpbooking"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            </div>
                        <label className="col-3 text-left" htmlFor="corpbooking">Corparate Booking</label><br/>
                        <div className="col-12 text-center">
                            <button className="btn btn-primary" onClick={this.handleSubmit} disabled={detailsAvail}>Submit</button>
                        </div>
            </div>
        </div>
        )
    }   
}
export default FlightBill;