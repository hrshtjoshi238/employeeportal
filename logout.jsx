import React,{Component} from 'react';
import auth from '../services/authservice';
class Logout extends Component{
    componentDidMount(){
        auth.logout();
        window.location="/login";
    }
    render(){
      return "";  
    }
}
export default Logout;