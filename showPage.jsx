import React,{Component} from 'react';
class ShowPage extends Component{
    render(){
        const {user}=this.props;
        console.log(user);
        return(
            <div className="container">
                {
                    user.role==="ADMIN"?<h3 className="text-center">Welcome! Admin</h3>
                    :<h3 className="text-center">Welcome {user.name} to the Employee Management Portal</h3>
                }
            </div>
        )
    }
} 
export default ShowPage;