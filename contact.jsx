import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservice';
class Contact extends Component{
    state={
        contactDetails:{
            country:"",
            city:"",
            pincode:"",
            address:"",
            mobile:""
        },
        errors:{},
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.contactDetails[input.name]=input.value;
        this.setState(s1);
    }
    validateAll=()=>{
        let {country,mobile,pincode}=this.state.contactDetails;
        let errors={};
        errors.country=this.validateCountry(country);
        errors.pincode=this.validatePinCode(pincode);
        errors.mobile=this.validateMobile(mobile);
        return errors;

    }
    validateCountry=(country)=>{
        return !country?"Country is not specified!!"
        :"";
    }
    validatePinCode=(pincode)=>{
        return !pincode?"Pincode is not specified!!"
        :"";
    }
    validateMobile=(mobile)=>{
        let passw = /^[\d\-+\s]+$/;
        return !mobile?"Mobile Number is not specified!!"
        :mobile.length<10?"Length of Mobile No is less than 10"
        :passw.test(mobile)===false?"Mobile number has at least 10 characters. Allowed are 0-9,+,-,space"
        :"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit = (e) =>{
        let user=auth.getUser();
        let {empuserid}=user;
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            //console.log(errors);
            this.addDetails(`/empapp/empcontact/${empuserid}`,this.state.contactDetails);
        }
    }
    async addDetails(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            let errors={mobile:"",pincode:"",country:""};
            //console.log(response);
            this.setState({success:data,detailsAvail:false,errors:errors});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    async componentDidMount(){
        let user=auth.getUser();
        let {empuserid}=user;
        //console.log(empuserid);
        let response=await http.get(`/empapp/empcontact/${empuserid}`);
        let {data}=response;
        //console.log(data);
        let detailsAvail=false;
        data.country&&data.pincode&&data.mobile?detailsAvail=true:detailsAvail=false;
        this.setState({contactDetails:data,detailsAvail:detailsAvail});
    }
    
    render(){
        let {contactDetails={},detailsAvail,errors,success,errors2}=this.state;
        let {country,city,pincode,address,mobile}=contactDetails;
        //console.log(contactDetails);
        return (
        <div className="container text-center">
            <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <h3 className="text-center">Your Contact Details</h3>
            <div className="row bg-light p-4">
            {
                    detailsAvail?<span className="col-12 text-success">Displaying Contact Details</span>:
                    success?<span className="col-12 text-success">Details Successfully Added</span>:
                    errors2?<span className="col-12 text-danger">{errors2}</span>:
                    <span className="col-12 text-primary">No Contact Details Found. Please Enter Them</span>
            }
                <div className="col-6 text-right"><label htmlFor="mobile">Mobile:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="mobile"
                        className="form-control"
                        type="text"
                        name="mobile"
                        placeholder="Enter Mobile Number"
                        value={mobile}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    {errors.mobile?<p className="text-danger text-left">{errors.mobile}</p>:""}
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="address">Address:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="address"
                        className="form-control"
                        type="text"
                        name="address"
                        placeholder="Enter Address"
                        value={address}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />    
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="city">City:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="city"
                        className="form-control"
                        type="text"
                        name="city"
                        placeholder="Enter City"
                        value={city}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="country">Country:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="country"
                        className="form-control"
                        type="text"
                        name="country"
                        placeholder="Enter Country"
                        value={country}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    {errors.country?<p className="text-danger text-left">{errors.country}</p>:""}
                    </div>
                </div>
                <div className="col-6 text-right"><label htmlFor="pincode">PinCode:</label></div>
                <div className="col-3">
                    <div className="form-group">
                        <input
                        id="pincode"
                        className="form-control"
                        type="text"
                        name="pincode"
                        placeholder="Enter PinCode"
                        value={pincode}
                        disabled={detailsAvail}
                        onChange={this.handleChange}
                        />
                    {errors.pincode?<p className="text-danger text-left">{errors.pincode}</p>:""}
                    </div>
                </div>
                <div className="col-12 text-center">
                    <button className="btn btn-primary" onClick={this.handleSubmit} disabled={detailsAvail}>Submit</button>
                </div>
            </div>
        </div>
        )
    }
}
export default Contact; 