import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Navbar,Nav,NavDropdown} from 'react-bootstrap';
class NavBar extends Component{
    render(){
        const {user}=this.props;
        return(
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Link className="navbar-brand" to="/">Employee Portal</Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  <Nav className="mr-auto">
                    {
                        user&&user.role==="ADMIN"&&<NavDropdown title="Admin" id="collasible-nav-dropdown">
                            <NavDropdown.Item><Link className="nav-link text-dark" to="/admin/addUser">Add Employee</Link></NavDropdown.Item>
                            <NavDropdown.Item><Link className="nav-link text-dark" to="/admin/viewemp">View Employee</Link></NavDropdown.Item>
                        </NavDropdown>
                    }
                    {
                        user&&user.role!="ADMIN"&&<NavDropdown title="My Portal" id="collasible-nav-dropdown">
                            <NavDropdown.Item><Link className="nav-link text-dark" to="/emp/contact">Contact Details</Link></NavDropdown.Item>
                            <NavDropdown.Item><Link className="nav-link text-dark" to="/emp/bills">Bills</Link></NavDropdown.Item>
                        </NavDropdown>
                    }
                  </Nav>
                  <Nav className="ml-auto">
                    {!user?<Link className="nav-link" to="/login">Login</Link>
                        :<Link className="nav-link" to="/logout">Logout</Link>
                    }
                  </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
export default NavBar;