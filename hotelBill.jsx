import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservice';
class HotelBill extends Component{
    state={
        hotelDetails:{
            billid:"",
            empuserid:"",
            staystartdate:{
                day:"",
                month:"",
                year:""
            }, 
            stayenddate:{
                day:"",
                month:"",
                year:""
            },
            city:"",
            hotel:"",
            corpbooking:""
        },
        years:["2018","2019","2020"],
        months:["January","February","March","April","May","June","July","August","September","October","November","December"],
        errors:{}
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        input.className.split(" ")[1]==="staystartdate"||input.className.split(" ")[1]==="stayenddate"?
            s1.hotelDetails[input.className.split(" ")[1]][input.name]=input.value
        :input.type==="checkbox"?
            input.checked?s1.hotelDetails[input.name]="Yes"
            :s1.hotelDetails[input.name]="No"
        :s1.hotelDetails[input.name]=input.value;
        this.setState(s1);
    }
    validateAll=()=>{
        let {city,hotel,staystartdate,stayenddate}=this.state.hotelDetails;
        let errors={};
        errors.city=this.validateCity(city);
        errors.hotel=this.validateHotel(hotel);
        errors.staystartdate=this.validateStayStartDate(staystartdate);
        errors.stayenddate=this.validateStayEndDate(stayenddate);
        return errors;

    }
    validateCity=(city)=>{
        return !city?"City is not entered":
        "";
    }
    validateHotel=(hotel)=>{
        return !hotel?"Hotel is not entered":"";
    }
    validateStayStartDate=(staystartdate)=>{
        let keys=Object.keys(staystartdate);
        return keys.reduce((acc,curr)=>!staystartdate[curr]?++acc:acc,0)?"Date is not selected properly":"";
    }
    validateStayEndDate=(stayenddate)=>{
        let keys=Object.keys(stayenddate);
        return keys.reduce((acc,curr)=>!stayenddate[curr]?++acc:acc,0)?"Date is not selected properly":"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit=(e)=>{
        let {billId}=this.props.match.params;
        e.preventDefault();
        let user=auth.getUser();
        let {empuserid}=user;
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            let s1={...this.state};
            let hotelDetails={
                billid:billId,
                empuserid:empuserid,
                hotel:s1.hotelDetails.hotel,
                city:s1.hotelDetails.city,
                staystartdate:s1.hotelDetails.staystartdate.day+"-"+s1.hotelDetails.staystartdate.month+"-"+s1.hotelDetails.staystartdate.year,
                stayenddate:s1.hotelDetails.stayenddate.day+"-"+s1.hotelDetails.stayenddate.month+"-"+s1.hotelDetails.stayenddate.year,
                corpbooking:s1.hotelDetails.corpbooking?"Yes":"No"
            }
            this.addDetails(`/empapp/hotelbill`,hotelDetails);
        }

    }
    async addDetails(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            let errors={};
            //console.log(response);
            this.setState({success:data,detailsAvail:false,errors:errors});
        }
        catch (ex){
            if(ex.response&&ex.response.status===401){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({errors2:errors2});
            }
        }
    }
    async componentDidMount(){
        let {billId}=this.props.match.params;
        let user=auth.getUser();
        let {empuserid}=user;
        //console.log(empuserid);
        try {
            let response=await http.get(`/empapp/hotelbill/${empuserid}/${billId}`);
            let {data}=response;
            let details={
                billid:billId,
                amount:data.amount,
                city:data.city,
                hotel:data.hotel,
                corpbooking:data.corpbooking,
                staystartdate:{
                    day:data.staystartdate.split("-")[0],
                    month:data.staystartdate.split("-")[1],
                    year:data.staystartdate.split("-")[2],
                },
                stayenddate:{
                    day:data.stayenddate.split("-")[0],
                    month:data.stayenddate.split("-")[1],
                    year:data.stayenddate.split("-")[2],
                }
            };
            this.setState({
                hotelDetails:details,
                detailsAvail:true
            });

            //console.log(data);
        }
        catch (ex){
            if(ex.response&&ex.response.status==500){
                let errors2={};
                //console.log(ex.response.data)
                errors2.adduser=ex.response.data;
                this.setState({detailsAvail:false});
            }
        }
    }
    getDays=()=>{
        let arr=[];
        for(let i=1;i<=31;i++){
            arr.push(i);
        }
        return arr;
    }
    render(){
        let {hotelDetails,years,months,detailsAvail,success,errors2,errors}=this.state;
        //console.log(detailsAvail);
        let {staystartdate,stayenddate,city,hotel,corpbooking}=hotelDetails;
        let days=this.getDays();
        let {billId}=this.props.match.params;
        return (
        <div className="container">
            <h3 className="text-center">Welcome To Employee Management Portal</h3>
            <h4 className="text-center">Hotel Stay Details</h4>
            <h6 className="text-center font-weight-bolder">Bill Id : {billId}</h6>
            <div className="row bg-light">
            {
                    detailsAvail?<span className="col-12 text-success text-center">Displaying Hotel Details</span>:
                    success?<span className="col-12 text-success text-center">Details Successfully Added</span>:
                    errors2?<span className="col-12 text-danger text-center">{errors2}</span>:
                    <span className="col-12 text-primary text-center">No Hotel Details Found. Please Enter Them</span>
            }
                <label className="col-6 text-right">Check in date:</label>
                    <div className="col-6">
                    <div className="row">
                    <div className="form-group col-4">
                        <select className="form-control staystartdate" disabled={detailsAvail} name="day" value={staystartdate.day} onChange={this.handleChange}>
                            <option disabled selected value="">Select Day</option>
                            {
                                days.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control staystartdate"  disabled={detailsAvail} name="month" value={staystartdate.month} onChange={this.handleChange}>
                            <option disabled selected value="">Select Month</option>
                            {
                                months.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control staystartdate" disabled={detailsAvail} name="year" value={staystartdate.year} onChange={this.handleChange}>
                            <option disabled selected value="">Select Year</option>
                            {
                                years.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    {
                        errors.staystartdate?<p className="text-danger text-left">{errors.staystartdate}</p>:""
                    }
                    </div>
                    </div>
                    <label className="col-6 text-right">Check out date:</label>
                    <div className="col-6">
                    <div className="row">
                    <div className="form-group col-4">
                        <select className="form-control stayenddate" disabled={detailsAvail} name="day" value={stayenddate.day} onChange={this.handleChange}>
                            <option disabled selected value="">Select Day</option>
                            {
                                days.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control stayenddate"  disabled={detailsAvail} name="month" value={stayenddate.month} onChange={this.handleChange}>
                            <option disabled selected value="">Select Month</option>
                            {
                                months.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group col-4">
                        <select className="form-control stayenddate"  disabled={detailsAvail} name="year" value={stayenddate.year} onChange={this.handleChange}>
                            <option disabled selected value="">Select Year</option>
                            {
                                years.map((x)=><option>{x}</option>)
                            }
                        </select>
                    </div>
                    {
                        errors.stayenddate?<p className="text-danger text-left">{errors.stayenddate}</p>:""
                    }
                    </div>
                    </div>
                    <label className="col-6 text-right" htmlFor="hotel">Hotel:</label>
                            <div className="form-group col-3"><input
                            className="form-control"
                            type="text"
                            id="hotel"
                            value={hotel}
                            name="hotel"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.hotel?<p className="text-danger text-left">{errors.hotel}</p>:""
                            }
                            </div>
                    <label className="col-6 text-right" htmlFor="city">City:</label>
                            <div className="form-group col-3"><input
                            className="form-control"
                            type="text"
                            id="city"
                            value={city}
                            name="city"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            {
                                errors.city?<p className="text-danger text-left">{errors.city}</p>:""
                            }
                            </div>
                        <div className="form-group text-right col-6"><input
                            className="form-checked"
                            type="checkbox"
                            id="corpbooking"
                            value={corpbooking}
                            checked={corpbooking==="Yes"}
                            name="corpbooking"
                            disabled={detailsAvail}
                            onChange={this.handleChange}/>
                            </div>
                        <label className="col-3 text-left" htmlFor="corpbooking">Corparate Booking</label><br/>
                        <div className="col-12 text-center">
                            <button className="btn btn-primary" onClick={this.handleSubmit} disabled={detailsAvail}>Submit</button>
                        </div>
                    
            </div>
        </div>
        )
    }   
}
export default HotelBill;